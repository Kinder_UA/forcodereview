//
//  LatestViewController.m
//  Messenger
//
//  Created by Igor Rotaru on 25.06.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "LatestViewController.h"
#import "ApiManager.h"
#import <MagicalRecord/MagicalRecord.h>
#import "ConversationTableViewCell.h"
#import "ConversationEntety.h"
#import "MessageEntety.h"
#import "HelperManager.h"
#import "DataManager.h"
#import "GTMNSString+HTML.h"
#import "MessagesViewController.h"
#import "NSUserDefaults+AppSettings.h"
#import "SettingEntity.h"

@interface LatestViewController () <NSFetchedResultsControllerDelegate/*, JSQMessagesViewControllerDelegate*/>
{
    NSNumber * memberID;
    NSDate * dateOfSeparation;
    BOOL playSound;
}

@property (weak) NSTimer *timer;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end

static NSString * const cellConversationIdentifier = @"conversationCellID";
static NSString * const fetchedControllerCasheName = @"conversationsCache";

static NSString * const segueIdentifierToMessages = @"segueToMessagesID";

@implementation LatestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setParams];
    playSound = NO;
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //NSComparisonResult result = [dateOfSeparation compare:[HelperManager sharedManager].dateOfSeparetion];
    
    //if(result == NSOrderedAscending || result == NSOrderedDescending){
        dateOfSeparation = [HelperManager sharedManager].dateOfSeparetion;
        [self updateFetchedResultsControllerResults];
    //}
    NSArray *tmp = [ConversationEntety MR_findAll];
    NSLog(@"%li", tmp.count);
    [self.tblConversation updateConstraints];
    [self.view layoutIfNeeded];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setParams
{
    [self setTimerConversations];
    
    
    dateOfSeparation = [HelperManager sharedManager].dateOfSeparetion;
    
    [NSFetchedResultsController deleteCacheWithName:fetchedControllerCasheName];
    NSError *error;
    if (![self.fetchedResultsController performFetch:&error]){
    }
    self.tblConversation.dataSource = self;
    self.tblConversation.delegate = self;
    
    
    NSInteger predicateParam = [[NSUserDefaults extraSettingUserMemberID] integerValue];
    SettingEntity * setting = [SettingEntity MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"userMemberID == %li",predicateParam]];
    [HelperManager setEnabledNotifications:[setting.sNotifications boolValue]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(invalidateTimers) name:notificationInvalidateTimers object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setTimerConversations) name:notificationFireTimers object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(photoDownloaded) name:notificationPhotoDownloaded object:nil];
}

- (void)onTick:(id)sender{
    [self loadLatestConversations];
}

- (void)setTimerConversations
{
    self.timer = [NSTimer scheduledTimerWithTimeInterval: [HelperManager sharedManager].requestInterval
                                             target: self
                                           selector:@selector(onTick:)
                                           userInfo: nil repeats:YES];
    [self.timer fire];
}


- (void)photoDownloaded{
    [self.tblConversation reloadData];
    NSLog(@"reload data");
    
}

- (void)invalidateTimers
{
    if(self.timer.valid){
        [self.timer invalidate];
        self.timer = nil;
    }
}


- (void)updateFetchedResultsControllerResults
{
    NSInteger predicateParam = [[NSUserDefaults extraSettingUserMemberID] integerValue];
    NSPredicate * conversationPredicate = [NSPredicate predicateWithFormat:@"userOwnerID == %li AND sentDateTime >= %@",predicateParam, dateOfSeparation];
    [self.fetchedResultsController.fetchRequest setPredicate:conversationPredicate];
    
    // Set the batch size to a suitable number.
    [self.fetchedResultsController.fetchRequest setFetchBatchSize:10];
    
    [NSFetchedResultsController deleteCacheWithName:fetchedControllerCasheName];
    NSError *error;
    if (![self.fetchedResultsController performFetch:&error]){
    }
    
    [self.tblConversation reloadData];
    
    [self.tblConversation layoutIfNeeded];
}

- (void)loadLatestConversations
{
    if([ApiManager sharedManager].networkReachabilityProperty && [ApiManager sharedManager].sessionId)
        [[ApiManager sharedManager] loadConversationsForPage:1 listConversations:[NSMutableArray new]];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:segueIdentifierToMessages])
    {
        UINavigationController *nc = segue.destinationViewController;
        MessagesViewController * vcMessages = (MessagesViewController*)nc.topViewController;
        vcMessages.conversationMemberID = memberID;
        //vcMessages.delegateModal = self;
    }
}



#pragma mark -  UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[_fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
    //NSLog(@"%li", [sectionInfo numberOfObjects]);
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ConversationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellConversationIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void) configureCell:(ConversationTableViewCell *)cell atIndexPath:(NSIndexPath*)indexPath {
    ConversationEntety * entity = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if([entity.senderID integerValue] == [entity.memberID integerValue])
        playSound = YES;
    UIImage *profileImage = [HelperManager imageWithFilename:entity.picture pathComponent:constPathToProfileImages];
    if(profileImage == nil){
        profileImage = [UIImage imageNamed:@"noImage"];
    }
    cell.imgAvatar.image = profileImage;
    cell.imgAvatar.layer.masksToBounds = YES;
    cell.imgAvatar.layer.cornerRadius = cell.imgAvatar.frame.size.width/2.0;
    cell.imgAvatar.layer.borderWidth = 2;
    cell.imgAvatar.layer.borderColor = [UIColor colorWithRed:253.0/255.0 green:111.0/255.0 blue:67.0/255.0 alpha:1.0].CGColor;
    
    cell.lblUserName.text = entity.login;
    
    NSString * shortContent = entity.shortContent;
    NSAttributedString *attrMsg = [HelperManager attributedMessageWithStr:shortContent andScale:1.5 andProtected:entity.hiddenAsUnpaid];
    cell.lblConversationContant.attributedText = attrMsg;
    //cell.lblConversationContant.text = [shortContent gtm_stringByUnescapingFromHTML];

    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *componentsSentDateMessage = [gregorianCalendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:entity.sentDateTime];
    NSDateComponents *componentsDateNow = [gregorianCalendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    NSString * strDateFormate = @"dd.MM.yy";
    if(componentsSentDateMessage.year == componentsDateNow.year
       && componentsSentDateMessage.month == componentsDateNow.month
       &&componentsSentDateMessage.day == componentsDateNow.day)
        strDateFormate = @"HH:mm";
    
    cell.lblSentDateMessage.text = [HelperManager convertDate:entity.sentDateTime toStringWithFormate:strDateFormate];

    if(![DataManager checkIsUserContactInDatabase:entity.memberID] && [ApiManager sharedManager].networkReachabilityProperty)
    {
        [[ApiManager sharedManager] loadUserDetailsInfo:entity.memberID completion:^(NSDictionary *dicData, NSError *error) {
            if(error) {
                /*[HelperManager showMessage:[HelperManager getResponceDescriptionWithKey:keyErrorInternetConnection]
                                 withTitle:[HelperManager getResponceDescriptionWithKey:keyErrorHeader]];
                 */
                return;
            } else  {
                [DataManager saveContact:dicData isFriend:NO];
            }
        }];
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ConversationEntety * entity = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TabBarStoryboard" bundle:nil];
 
    MessagesViewController *vcMessages = [storyboard instantiateViewControllerWithIdentifier:@"messagesViewController"];
    vcMessages.conversationMemberID = entity.memberID;//memberID;
    //vcMessages.delegateModal = self;
    
    //[HelperManager presentChildVC:vcMessages withParrentVC:self];
    [self.navigationController pushViewController:vcMessages animated:YES];
    
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([ConversationEntety class])];
    
    NSInteger predicateParam = [[NSUserDefaults extraSettingUserMemberID] integerValue];
    NSPredicate * conversationPredicate = [NSPredicate predicateWithFormat:@"userOwnerID == %li AND sentDateTime >= %@",predicateParam, dateOfSeparation];
   [fetchRequest setPredicate:conversationPredicate];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:10];
    
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"sentDateTime" ascending:NO];
    fetchRequest.sortDescriptors = @[descriptor];
    
    NSFetchedResultsController *theFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:context sectionNameKeyPath:nil cacheName:fetchedControllerCasheName];
    self.fetchedResultsController = theFetchedResultsController;
    self.fetchedResultsController.delegate = self;
    
    return self.fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tblConversation beginUpdates];
    
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tblConversation insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tblConversation deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tblConversation;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(ConversationTableViewCell *)[tableView cellForRowAtIndexPath:indexPath]
                    atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
    
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tblConversation endUpdates];
    
}












@end
