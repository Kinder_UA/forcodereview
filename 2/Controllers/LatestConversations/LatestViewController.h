//
//  LatestViewController.h
//  Messenger
//
//  Created by Igor Rotaru on 25.06.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LatestViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView * tblConversation;

@end
