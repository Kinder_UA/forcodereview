//
//  LoginViewController.m
//
//
//  Created by User on 16.07.15.
//  
//

#import "LoginViewController.h"
#import "Constants.h"
#import "HelperManager.h"
#import "ApiManager.h"
#import "DataManager.h"
#import <MBProgressHUD.h>
#import "NSUserDefaults+Settings.h"
#import "UserClass.h"

#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface LoginViewController (){
    UITextField * activeField;
    BOOL isAutologin;
    BOOL isEnterFromRegistraionFB;
}

@end


NSString * const segueToRegisterUser = @"segueToRegUserID";

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setParams];
    [HelperManager clearAllCookies];
    [self loadBaseInformation];
    
    // Autologin
    isAutologin = NO;
    if([NSUserDefaults isSetSettingRememberMe] && [NSUserDefaults extraSettingRememberMe])
    {
        if([NSUserDefaults extraSettingUserLoginFB]){
            [self actionLoginUserFB:@{@"id": [NSUserDefaults extraSettingUserLoginFB]}];
            return;
        }
        
        isAutologin = YES;
        [self actionLoginUser:nil];
    }
    
    if(isAutologin){
        NSString *strLogin = [NSUserDefaults extraSettingUserLogin];
        NSString *strPassword = [NSUserDefaults extraSettingUserPassword];
        [self loginWithUsername:strLogin andPassword:strPassword];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.view layoutIfNeeded];
    
    self.constrInputHeight.constant = self.btnFB.frame.size.width/[HelperManager sharedManager].defaultInputRatio;
    self.constrLoginButtonHeight.constant = self.btnLogin.frame.size.width/6.33;
    self.constrRgistrationButtonWidth.constant = self.btnRegistration.frame.size.height*2.7;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(![NSUserDefaults isSetSettingRememberMe])
    {
        isAutologin = NO;
    }

}

//#warning For test
//- (BOOL)application:(UIApplication *)application
//            openURL:(NSURL *)url
//  sourceApplication:(NSString *)sourceApplication
//         annotation:(id)annotation {
//    return [[FBSDKApplicationDelegate sharedInstance] application:application
//                                                          openURL:url
//                                                sourceApplication:sourceApplication
//                                                       annotation:annotation];
//}

- (void)setParams
{
    [self registerForKeyboardNotifications];
    
    NSString * forgotString = self.lblRecoverPassword.text;
    self.lblRecoverPassword.attributedText = [HelperManager setUnderlineAttributeText:forgotString fontName:constFontRobotoRegular fontSize:11.0f];
    
    self.tfEmail.delegate = self;
    self.tfPassword.delegate = self;
    
    for(UIView * vw in self.vwListBorder)
    {
        vw.layer.cornerRadius = 2.0f;
        vw.layer.borderWidth = 1.5f;
        vw.layer.borderColor = [HelperManager sharedManager].defaultColor.CGColor;
        
        if([vw isKindOfClass:[UITextField class]]){
            UITextField * tf = (UITextField*)vw;
            tf.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 12, 12)];
            tf.leftViewMode = UITextFieldViewModeAlways;
        }
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterRegUserFB:) name:notificationRegFB object:nil];
}

- (void)loadBaseInformation
{
    if(![DataManager sharedManager].listCities || ![DataManager sharedManager].listSalonsType)
    {
        [[ApiManager sharedManager] getListCities:nil];
        [[ApiManager sharedManager] getListSalonTypes:nil];
    }
}

- (void)enterRegUserFB:(NSNotification*)notification
{
    isEnterFromRegistraionFB = YES;
    NSDictionary * dicInfo = @{@"id": (NSString*)[notification object]};
    
    [self actionLoginUserFB:dicInfo];
}


- (void)loginWithUsername:(NSString *)username andPassword:(NSString *)password{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TabBar" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"rootTabBarViewController"];
    
    __weak typeof(self) weakSelf = self;
    
    if([HelperManager isValidInputBox:username]
       && [HelperManager isValidInputBox:password]
       && [HelperManager isValidEmail:username])
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        [[ApiManager sharedManager] loginUser:username
                                     password:password
                                   completion:^(id dicData, NSError *error) {
                                       [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
                                       if(error){
                                           //[HelperManager showMessage:(NSString*)dicData withTitle:constErrorTitle];
                                           return;
                                       }
                                       
                                       [[ApiManager sharedManager] setToken:dicData[@"token"]];
                                       
                                       [NSUserDefaults saveSettingUserPassword:password];
                                       [NSUserDefaults saveSettingUserLogin:username];
                                       if(!isAutologin)
                                           [NSUserDefaults saveSettingRememberMe:weakSelf.lblRememberMeOff.hidden];
                                       
                                       [DataManager sharedManager].userInApp = [[UserClass alloc] initWithDicData:dicData[@"user"]];
                                       
                                       [weakSelf.navigationController pushViewController:vc animated:YES];
                                   }];
    }

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:segueToRegisterUser])
    {
        
    }
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField becomeFirstResponder];
    activeField = nil;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if([textField isEqual:self.tfEmail])
        [self.tfPassword becomeFirstResponder];
    
    return YES;
}

#pragma mark - Keyboard

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrView.contentInset = contentInsets;
    self.scrView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [self.scrView scrollRectToVisible:activeField.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrView.contentInset = contentInsets;
    self.scrView.scrollIndicatorInsets = contentInsets;
}

#pragma mark - Actions

- (IBAction)actionRemeberMe:(UIButton*)btnSender
{
    btnSender.selected = !btnSender.selected;
    
    NSString * imgName = btnSender.selected ? @"btnSliderOn" : @"btnSliderOff";
    self.imgRememberMe.image = [UIImage imageNamed:imgName];
    
    self.lblRememberMeOff.hidden = btnSender.selected;
    self.lblRememberMeOn.hidden = !btnSender.selected;
    
    if(!btnSender.selected){
        [NSUserDefaults deleteSettingRememberMe];
        isAutologin = NO;
    }
}

- (IBAction)actionLoginUser:(id)sender
{
    BOOL isValid = YES;
    NSString * strError;
    
    if(![HelperManager isValidEmail:self.tfEmail.text]){
        isValid = NO;
        strError = strError == nil ? constErrorIncorrectEmail : strError;
        //strError = constErrorIncorrectEmail;
    }
    
    if(![HelperManager isValidInputBox:self.tfPassword.text]){
        isValid = NO;
        strError = strError == nil ? constErrorIncorrectPassword : strError;
    }
    
    if(!isValid){
        [HelperManager showMessage:strError withTitle:constErrorTitle];
        return;
    }
    
    [self loginWithUsername:self.tfEmail.text andPassword:self.tfPassword.text];

    /*UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TabBar" bundle:nil];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"rootTabBarViewController"];
    
    __weak typeof(self) weakSelf = self;
    NSString * strLogin = self.tfEmail.text;
    NSString * strPassword = self.tfPassword.text;
    
    if(isAutologin){
        strLogin = [NSUserDefaults extraSettingUserLogin];
        strPassword = [NSUserDefaults extraSettingUserPassword];
    }
    
    if([HelperManager isValidInputBox:strLogin]
         && [HelperManager isValidInputBox:strPassword]
         && [HelperManager isValidEmail:strLogin])
    {
        [[ApiManager sharedManager] loginUser:strLogin
                                     password:strPassword
                                   completion:^(NSDictionary *dicData, NSError *error) {
                                       if(error){
                                           return;
                                       }
                                       [[ApiManager sharedManager] setToken:dicData[@"token"]];
                        
                                       [NSUserDefaults saveSettingUserPassword:strPassword];
                                       [NSUserDefaults saveSettingUserLogin:strLogin];
                                       if(!isAutologin)
                                           [NSUserDefaults saveSettingRememberMe:weakSelf.lblRememberMeOff.hidden];
                                       
                                       [DataManager sharedManager].userInApp = [[UserClass alloc] initWithDicData:dicData[@"user"]];
                                       
                                       [weakSelf.navigationController pushViewController:vc animated:YES];
                                   }];
    }*/
}

- (IBAction)actionRegistrationUser
{
    /*if(![DataManager sharedManager].listCities || ![DataManager sharedManager].listSalons)
    {
        __weak typeof(self) weakSelf = self;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [[ApiManager sharedManager] getListCities:^(NSDictionary *dicData, NSError *error) {
            
            [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
            
            if(!error)
                [weakSelf performSegueWithIdentifier:segueToRegisterUser sender:nil];
        }];
    } else {
        [self performSegueWithIdentifier:segueToRegisterUser sender:nil];
    }*/
    
    [self performSegueWithIdentifier:segueToRegisterUser sender:nil];
}

- (IBAction)actionLoginUserFB:(id)sender
{
    __weak typeof(self) weakSelf = self;
    
    CompletionBlock localBlock = ^(id dicData, NSError * error){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        NSString * userFbId = [dicData objectForKey:@"id"];
        NSString * firstName = [dicData objectForKey:@"first_name"] ? [dicData objectForKey:@"first_name"] : @" ";
        NSString * secondName = [dicData objectForKey:@"last_name"] ? [dicData objectForKey:@"last_name"] : @" ";
        
        NSMutableDictionary * dicUserInfo = [NSMutableDictionary dictionaryWithDictionary:@{@"firstName": firstName,
                                                                                            @"secondName": secondName,
                                                                                            @"fbId": dicData[@"id"]}];
        
        NSString * email = [dicData objectForKey:@"email"] ? [dicData objectForKey:@"email"] : nil;
        if(email)
            [dicUserInfo setValue:email forKey:@"email"];
        
        [[ApiManager sharedManager] loginUserFBParams:dicUserInfo completionBlock:^(id dicData, NSError *error)
         {
             [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
             if(error){
                 [HelperManager showMessage:error.localizedDescription withTitle:constErrorTitle];
                 return;
             }
             
             UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"TabBar" bundle:nil];
             UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"rootTabBarViewController"];
             
             [[ApiManager sharedManager] setToken:dicData[@"token"]];
             
             if(!isAutologin)
                 [NSUserDefaults saveSettingRememberMe:weakSelf.lblRememberMeOff.hidden];
             
             [DataManager sharedManager].userInApp = [[UserClass alloc] initWithDicData:dicData[@"user"]];
             [NSUserDefaults saveSettingUserLoginFB:userFbId];
             
             [weakSelf.navigationController pushViewController:vc animated:YES];
         }];
    };

    
    [[ApiManager sharedManager] loginFB:^(id dicData, NSError *error) {
        if(error){
        [HelperManager showMessage:error.localizedDescription withTitle:constErrorTitle];
             return;
        }
         
        //[DataManager sharedManager].userInApp = [[UserClass alloc] init];
         
        localBlock(dicData, error);
    }];
}

- (IBAction)actionShowHidePassword:(UIButton*)sender
{
    static BOOL isShowingPassword = NO;
    isShowingPassword = !isShowingPassword;
    
    sender.selected = isShowingPassword;
    UITextField *textField = self.tfPassword;
    textField.secureTextEntry = !isShowingPassword;
}



@end
