//
//  LoginViewController.h
//  
//
//  Created by User on 16.07.15.
//  Copyright (c) 2015 Igor Rotaru. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray * vwListBorder;

@property (strong, nonatomic) IBOutlet UIButton * btnFB;
@property (strong, nonatomic) IBOutlet UITextField * tfEmail;
@property (strong, nonatomic) IBOutlet UITextField * tfPassword;
@property (strong, nonatomic) IBOutlet UIButton * btnRecoverPassword;
@property (strong, nonatomic) IBOutlet UILabel * lblRecoverPassword;
@property (strong, nonatomic) IBOutlet UILabel * lblEmail;
@property (strong, nonatomic) IBOutlet UILabel * lblPassword;

@property (strong, nonatomic) IBOutlet UILabel * lblRememberMe;
@property (strong, nonatomic) IBOutlet UIButton * btnRememberMe;
@property (strong, nonatomic) IBOutlet UIImageView * imgRememberMe;
@property (strong, nonatomic) IBOutlet UILabel * lblRememberMeOn;
@property (strong, nonatomic) IBOutlet UILabel * lblRememberMeOff;

@property (strong, nonatomic) IBOutlet UIButton * btnRegistration;
@property (strong, nonatomic) IBOutlet UIButton * btnLogin;

@property (strong, nonatomic) IBOutlet UIScrollView * scrView;
@property (strong, nonatomic) IBOutlet UIView * vwContent;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constrInputHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constrLoginButtonHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constrRgistrationButtonWidth;

@end
