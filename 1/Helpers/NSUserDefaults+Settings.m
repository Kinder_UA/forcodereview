//
//  NSUserDefaults+Settings.m
//  Bellbox
//
//  Created by User on 20.07.15.
//  Copyright (c) 2015 Igor Rotaru. All rights reserved.
//

#import "NSUserDefaults+Settings.h"
#import "HelperManager.h"

static NSString * const kSettingRememberMe      = @"kSettingRememberMe";
static NSString * const kSettingUserAvatar      = @"kSettingUserAvatar";
static NSString * const kSettingUserLogin       = @"kSettingUserLogin";
static NSString * const kSettingUserLoginFB     = @"kSettingUserLoginFB";
static NSString * const kSettingUserPassword    = @"kSettingUserPassword";
static NSString * const kSettingPNS             = @"kSettingPNS";

static NSString * const constUserAvatarPath = @"UserAvatarPath";

@implementation NSUserDefaults (Settings)

#pragma mark - kSettingRememberMe

+ (void)saveSettingRememberMe:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:kSettingRememberMe];
}

+ (BOOL)extraSettingRememberMe
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kSettingRememberMe] ;
}

+ (BOOL)isSetSettingRememberMe
{
    if([[NSUserDefaults standardUserDefaults] valueForKey:kSettingRememberMe])
        return YES;
    return NO;
}

+ (void)deleteSettingRememberMe
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kSettingRememberMe];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - kSettingUserAvatar

+ (void)saveSettingUserAvatar:(UIImage*)value
{
    [HelperManager saveImage:value withFilename:constUserAvatar pathComponent:constUserAvatarPath];
}

+ (UIImage*)extraSettingUserAvatar
{
    if([HelperManager isFileExists:constUserAvatar pathComponent:constUserAvatarPath])
       return [HelperManager imageWithFilename:constUserAvatar pathComponent:constUserAvatarPath];
    
    return nil;
}

+ (void)deleteSettingUserAvatar
{
    [HelperManager deleteImageWithFilename:constUserAvatar pathComponent:constUserAvatarPath];
}

#pragma mark - kSettingUserLogin

+ (void)saveSettingUserLogin:(NSString*)value
{
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:kSettingUserLogin];
}

+ (NSString*)extraSettingUserLogin
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:kSettingUserLogin];
}

+ (void)deleteSettingUserLogin
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kSettingUserLogin];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - kSettingUserPassword

+ (void)saveSettingUserPassword:(NSString*)value
{
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:kSettingUserPassword];
}

+ (NSString*)extraSettingUserPassword
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:kSettingUserPassword];
}

+ (void)deleteSettingUserPassword
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kSettingUserPassword];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - kSettingUserLoginFB

+ (void)saveSettingUserLoginFB:(NSString*)value
{
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:kSettingUserLoginFB];
}

+ (NSString*)extraSettingUserLoginFB
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:kSettingUserLoginFB];
}

+ (void)deleteSettingUserLoginFB
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kSettingUserLoginFB];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - kSettingPNS

+ (void)saveSettingPNS:(BOOL)pnsEnabled
{
    [[NSUserDefaults standardUserDefaults] setValue:@(pnsEnabled) forKey:kSettingPNS];
    if(pnsEnabled){
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [[UIApplication sharedApplication]  registerForRemoteNotifications];
    }
    else{
        [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    }
}

+ (BOOL)extraSettingPNS
{
    return [[[NSUserDefaults standardUserDefaults] valueForKey:kSettingPNS] boolValue];
}

+ (void)deleteSettingPNS
{
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kSettingPNS];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


@end
