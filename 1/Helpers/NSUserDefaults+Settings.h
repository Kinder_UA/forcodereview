//
//  NSUserDefaults+Settings.h
//  Bellbox
//
//  Created by User on 20.07.15.
//  Copyright (c) 2015 Igor Rotaru. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface NSUserDefaults (Settings)

#pragma mark - kSettingRememberMe

+ (void)saveSettingRememberMe:(BOOL)value;
+ (BOOL)extraSettingRememberMe;
+ (BOOL)isSetSettingRememberMe;
+ (void)deleteSettingRememberMe;

#pragma mark - kSettingUserAvatar

+ (void)saveSettingUserAvatar:(UIImage*)value;
+ (UIImage*)extraSettingUserAvatar;
+ (void)deleteSettingUserAvatar;

#pragma mark - kSettingUserLogin

+ (void)saveSettingUserLogin:(NSString*)value;
+ (NSString*)extraSettingUserLogin;
+ (void)deleteSettingUserLogin;

#pragma mark - kSettingUserPassword

+ (void)saveSettingUserPassword:(NSString*)value;
+ (NSString*)extraSettingUserPassword;
+ (void)deleteSettingUserPassword;

#pragma mark - kSettingUserLoginFB

+ (void)saveSettingUserLoginFB:(NSString*)value;
+ (NSString*)extraSettingUserLoginFB;
+ (void)deleteSettingUserLoginFB;

#pragma mark - kSettingUserLoginFB

+ (void)saveSettingPNS:(BOOL)pnsEnabled;
+ (BOOL)extraSettingPNS;
+ (void)deleteSettingPNS;


@end
